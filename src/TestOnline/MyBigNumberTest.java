package TestOnline;

import static org.junit.Assert.assertEquals;

import javax.annotation.processing.Generated;

import org.junit.Test;

@Generated(value = "org.junit-tools-1.1.0")
public class MyBigNumberTest {

	private MyBigNumber createTestSubject() {
		return new MyBigNumber();
	}

	@Test
	public void testSum() throws Exception {
		MyBigNumber testSubject;
		String stn1 = "123456";
		String stn2 = "879";
		String result;
		int expected = Integer.parseInt(stn1) + Integer.parseInt(stn2);

		// default test
		testSubject = createTestSubject();
		result = testSubject.sum(stn1, stn2);
		assertEquals(expected, Integer.parseInt(result));
		
	}

}