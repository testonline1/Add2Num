package TestOnline;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(

{ MyBigNumberTest.class })
public class TestSuite { // nothing
}
